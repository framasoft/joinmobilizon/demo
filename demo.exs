defmodule Mobilizon.Demo do
  alias Mobilizon.{Actors, Addresses, Discussions, Events, Medias, Posts, Users}
  alias Mobilizon.Actors.Actor
  alias Mobilizon.Discussions.Comment
  alias Mobilizon.Events.{Event, Participant}
  alias Mobilizon.Medias.Media
  alias Mobilizon.Posts.Post
  alias Mobilizon.Service.Geospatial
  alias Mobilizon.Storage.Repo
  alias Mobilizon.Users.User

  def truncate(schema) do
    table_name = schema.__schema__(:source)
    Repo.query!("TRUNCATE #{table_name} CASCADE", [])
    :ok
  end

  def create_user(email, password) do
    with {:ok, %User{} = user} <- Users.register(%{email: email, password: password}) do
      user
    end
  end

  def create_person(user_id, username, name, summary \\ nil, avatar \\ nil) do
    with {:ok, %Actor{} = person} <-
           Actors.new_person(%{
             user_id: user_id,
             preferred_username: username,
             name: name,
             summary: summary,
             avatar: avatar
           }) do
      person
    end
  end

  def create_group(creator_actor_id, username, name, summary \\ nil, avatar \\ nil, banner \\ nil) do
    with {:ok, %Actor{} = group} <-
           Actors.create_group(%{
             creator_actor_id: creator_actor_id,
             preferred_username: username,
             name: name,
             summary: summary,
             avatar: avatar,
             banner: banner,
             visibility: :public
           }) do
      group
    end
  end

  def create_event(
        organizer_actor,
        group,
        title,
        body,
        address,
        begins_on,
        ends_on,
        event_options,
        picture,
        medias,
        language,
        category,
        tags
      ) do
    {:ok, %Event{} = event} =
      Events.create_event(%{
        organizer_actor_id: organizer_actor.id,
        attributed_to_id: if(group, do: group.id, else: nil),
        title: title,
        description: body,
        physical_address_id: address.id,
        begins_on: begins_on,
        ends_on: ends_on,
        options: event_options,
        picture_id: picture.id,
        media: medias,
        language: language,
        category: category,
        tags: tags
      })

    event
  end

  def create_participant(event, actor, metadata \\ nil) do
    {:ok, %Participant{} = participant} =
      Events.create_participant(%{event_id: event.id, actor_id: actor.id, metadata: metadata})

    participant
  end

  def create_post(author, group, title, body, picture \\ nil, medias \\ [], tags \\ []) do
    {:ok, %Post{} = post} =
      Posts.create_post(%{
        title: title,
        body: body,
        author_id: author.id,
        attributed_to_id: group.id,
        picture_id: if(picture, do: picture.id, else: nil),
        media: medias,
        tags: tags
      })

    post
  end

  def search_address(query) do
    case create_address(query) do
      {:ok, address} ->
        address

      {:error, err} ->
        IO.puts(inspect(err, pretty: true))
        nil
    end
  end

  defp create_address(query) do
    case Geospatial.service().search(query,
           lang: "fr"
         ) do
      [] ->
        nil

      addresses ->
        addresses
        |> hd()
        |> Map.from_struct()
        |> Addresses.create_address()
    end
  end

  def create_tag(tag) do
    case Mobilizon.Events.get_tag_by_slug(tag) do
      nil ->
        {:ok, tag} = Mobilizon.Events.create_tag(%{title: tag})
        tag

      tag ->
        tag
    end
  end

  def create_comment(author, event, body, in_reply_to_comment_id \\ nil, origin_comment_id \\ nil) do
    {:ok, %Comment{} = comment} =
      Discussions.create_comment(%{
        text: body,
        actor_id: author.id,
        event_id: event.id,
        in_reply_to_comment_id: in_reply_to_comment_id,
        origin_comment_id: origin_comment_id
      })

    comment
  end

  def upload_media(actor, path) do
    %{
      name: _name,
      url: url,
      content_type: content_type,
      size: size
    } = uploaded = upload_file(path)

    args =
      %{name: path}
      |> Map.put(:url, url)
      |> Map.put(:size, size)
      |> Map.put(:content_type, content_type)

    {:ok, media = %Media{}} =
      Medias.create_media(%{
        file: args,
        actor_id: actor.id,
        metadata: Map.take(uploaded, [:width, :height, :blurhash])
      })

    media
  end

  defp upload_file(path) do
    body = File.read!(Path.join("demo/uploads", path))

    {:ok, file} = Mobilizon.Web.Upload.store(%{body: body, name: path})
    file
  end

  def upload_direct_file(path) do
    path
    |> upload_file()
    |> Map.from_struct()
    |> Map.take([:name, :url, :content_type, :size])
  end
end

alias Mobilizon.Addresses.Address
alias Mobilizon.Actors.Actor
alias Mobilizon.Events.Event
alias Mobilizon.Users.User
alias Mobilizon.Demo

Demo.truncate(Address)
Demo.truncate(Event)
Demo.truncate(Actor)
Demo.truncate(User)

# Users
pouhiou =
  Demo.create_user("pouhiou+test@framasoft.org", System.get_env("DEMO_PASSWORD_POUHIOU_TEST"))

rose = Demo.create_user("spf+rose@framasoft.org", System.get_env("DEMO_PASSWORD_ROSE_TEST"))
pheobe = Demo.create_user("spf+pheobe@framasoft.org", System.get_env("DEMO_PASSWORD_PHEOBE_TEST"))
boreal = Demo.create_user("spf+boreal@framasoft.org", System.get_env("DEMO_PASSWORD_BOREAL_TEST"))
autral = Demo.create_user("spf+autral@framasoft.org", System.get_env("DEMO_PASSWORD_AUTRAL_TEST"))

# Profiles
_famille_autral = Demo.create_person(autral.id, "famille_autral", "Famille Autral")

valerian_austral =
  Demo.create_person(
    autral.id,
    "valerian_austral",
    "Valerian Austral",
    nil,
    Demo.upload_direct_file("valerian_austral.png")
  )

petunia_austral = Demo.create_person(autral.id, "petunia_austral", "Pétunia Austral")

david_bouleau =
  Demo.create_person(
    pouhiou.id,
    "david_bouleau",
    "David Bouleau",
    "J'écoute. J'essaie de me taire, d'apprendre et d'agir."
  )

david_birch =
  Demo.create_person(
    pouhiou.id,
    "david_birch",
    "David Birch",
    "I listen. I try to keep quiet, to learn and then act."
  )

geraldine_erable =
  Demo.create_person(
    pouhiou.id,
    "geraldine_erable",
    "Géraldine Érable",
    "La vie telle qu'elle devrait être : des ami·es, de l'art, de la musique, de l'amour, du temps."
  )

bergamote_austral = Demo.create_person(autral.id, "bergamote_austral", "Bergamote Austral")
_hibiscus_austral = Demo.create_person(autral.id, "hibiscus_austral", "Hibiscus Austral")
_iris_austral = Demo.create_person(autral.id, "iris_austral", "Iris Austral")

rose_utopia =
  Demo.create_person(
    rose.id,
    "rose_utopia",
    "Rȯse Utopia",
    "To utopia and beyond!",
    Demo.upload_direct_file("lily_austral.png")
  )

lily_austral =
  Demo.create_person(
    autral.id,
    "lily_austral",
    "Lily Austral",
    nil,
    Demo.upload_direct_file("lily_austral.png")
  )

rose_boreal = Demo.create_person(boreal.id, "rose_boreal", "Rȯse Boreal", "Family matter.")

benedict_green =
  Demo.create_person(
    pouhiou.id,
    "benedict_green",
    "Benedict Green",
    "Person who tries to take care of the world that surrounds them, of the beings that inhabit it and of the little me who is part of it."
  )

benedicte_verte =
  Demo.create_person(
    pouhiou.id,
    "benedicte_verte",
    "Bénédicte Verte",
    "Essaye de prendre soin du monde qui l'entoure, des être qui l'habitent et de moi qui en fait partie."
  )

calendula_austral =
  Demo.create_person(
    autral.id,
    "calendula_austral",
    "Calendula Austral",
    nil,
    Demo.upload_direct_file("calendula_austral.png")
  )

gardenia_boreal =
  Demo.create_person(
    boreal.id,
    "gardenia_boreal",
    "Gardénia Boreal",
    nil,
    Demo.upload_direct_file("gardenia_boreal.png")
  )

_pheobe_profile =
  Demo.create_person(pheobe.id, "pheobe", "Pheobe", "Envie de me faire des ami⋅e⋅s !")

_nadia = Demo.create_person(pheobe.id, "nadia", "Nadia")
_famille_boreal = Demo.create_person(boreal.id, "famille_boreal", "Famille Boreal")
acacia_boreal = Demo.create_person(boreal.id, "acacia_boreal", "Acacia Boreal")

fuchsia_boreal =
  Demo.create_person(
    boreal.id,
    "fuchsia_boreal",
    "Fuchsia Boreal",
    nil,
    Demo.upload_direct_file("fuchsia_boreal.png")
  )

jasmin_boreal =
  Demo.create_person(
    boreal.id,
    "jasmin_boreal",
    "Jasmin Boreal",
    nil,
    Demo.upload_direct_file("jasmin_boreal.png")
  )

geraldine_mapple =
  Demo.create_person(
    pouhiou.id,
    "geraldine_mapple",
    "Geraldine Mapple",
    "Life as it should be: all friends, all art, all music, all love, all the time."
  )

narcisse_boreal =
  Demo.create_person(
    boreal.id,
    "narcisse_boreal",
    "Narcisse Boreal",
    "Iris's husband. This account is for her birthday event (for now…)!",
    Demo.upload_direct_file("narcisse_boreal.jpg")
  )

# Groups
the_contributopists =
  Demo.create_group(
    benedict_green.id,
    "the_contributopists",
    "The ContribUtopists",
    "Let's contribute to actions that aims to make our utopias come true!",
    Demo.upload_direct_file("contributopia planete-educ pop.png"),
    Demo.upload_direct_file("0_Main-menu_by-David-Revoy.jpg")
  )

les_contributopistes =
  Demo.create_group(
    rose_utopia.id,
    "les_contributopistes",
    "Les ContribUtopistes",
    "Contribuons ensemble à des actions qui visent à concrétiser nos utopies !",
    Demo.upload_direct_file("contributopia planete-educ pop.png"),
    Demo.upload_direct_file("0_Main-menu_by-David-Revoy.jpg")
  )

_urban_collage =
  Demo.create_group(david_birch.id, "urban_collage", "Urban collage", "Urban space for people!")

# Events
lyon = Demo.search_address("10 Rue Jangot, Lyon")

picture = Demo.upload_media(narcisse_boreal, "photo-1530103862676-de8c9debad1d.webp")
media = Demo.upload_media(narcisse_boreal, "iris-austral.png")

anniversaire_iris =
  Demo.create_event(
    narcisse_boreal,
    nil,
    "Anniversaire d’Iris !",
    """
    <p>Certain⋅e⋅s l&#39;appellent Iris, d&#39;autres « maman » : quoiqu&#39;il en soit <strong>c&#39;est bientôt son anniversaire</strong> !</p><p></p><h1>🎉🎉 Fêtons cela dignement ! 🎉🎉</h1><p><img src="#{media.file.url}"/></p><p></p><h2>La dégustation du gâteau et l&#39;ouverture des cadeaux auront lieu à 17h30 !</h2><p></p><h1>🎂🎂 Auberge espagnole 🎂🎂</h1><p></p><p>Auto-gestion alimentaire ! <strong>Merci de venir avec de quoi manger et/ou boire !</strong></p><p></p><h3>🛏️ Hébergement 🛏️</h3><p></p><p>Possibilité de dormir sur place :)</p>
    """,
    lyon,
    ~U[2026-11-04 23:00:00Z],
    ~U[2026-11-05 20:00:00Z],
    %{comment_moderation: :closed},
    picture,
    [media],
    "fr",
    "PARTY",
    []
  )

Demo.create_participant(anniversaire_iris, rose_boreal)

picture = Demo.upload_media(narcisse_boreal, "photo-1530103862676-de8c9debad1d.webp")
media = Demo.upload_media(narcisse_boreal, "iris-austral.png")

birthday =
  Demo.create_event(
    narcisse_boreal,
    nil,
    "Iris's birthday!",
    """
    <p>Some call her Iris, others "mum": anyway <strong>it's her birthday soon</strong>!</p><p></p><h1>🎉🎉 Let's celebrate! 🎉🎉</h1><p><img src="#{media.file.url}"/></p><p></p><h2>The cake tasting and the opening of the gifts will take place at 5.30 pm!</h2><p></p><h1>🎂🎂 Self-management 🎂🎂</h1><p></p><p><strong>Please come with something to eat and/or drink!</strong></p><p></p><h3>🛏️ Hosting 🛏️</h3><p></p><p>Possibility to sleep on site :)</p>
    """,
    lyon,
    ~U[2026-11-04 23:00:00Z],
    ~U[2026-11-05 20:00:00Z],
    %{comment_moderation: :closed},
    picture,
    [media],
    "en",
    "PARTY",
    []
  )

Demo.create_participant(birthday, gardenia_boreal)
Demo.create_participant(birthday, acacia_boreal)
Demo.create_participant(birthday, fuchsia_boreal)
Demo.create_participant(birthday, jasmin_boreal)
Demo.create_participant(birthday, lily_austral)
Demo.create_participant(birthday, bergamote_austral)
Demo.create_participant(birthday, calendula_austral)
Demo.create_participant(birthday, rose_boreal)

boreal_birthday_comment =
  Demo.create_comment(jasmin_boreal, birthday, "<p>Sure! I'll be there!</p>")

Demo.create_comment(
  fuchsia_boreal,
  birthday,
  "<p><span class=\"mention\"><span class='h-card mention' data-user='694'>@<span>jasmin_boreal</span></span></span> Me too!</p>",
  boreal_birthday_comment.id,
  boreal_birthday_comment.id
)

Demo.create_comment(lily_austral, birthday, "<p>I'll definitly be there!</p>")
Demo.create_comment(calendula_austral, birthday, "<p>I'll bring beetroots!</p>")

picture = Demo.upload_media(rose_utopia, "2020-10-06-mobilizon-illustration-D_realisation.jpg")
collage = Demo.create_tag("collage")
streetart = Demo.create_tag("streetart")

collage_fr =
  Demo.create_event(
    rose_utopia,
    les_contributopistes,
    "Collage pas sage !",
    """
    <h1>Parce que la rue est à tout le monde, elle est aussi à nous toutes et tous !</h1><p></p><p>Profitons-en pour l&#39;<strong>embellir</strong> ! Pour la rendre <strong>politique</strong> ! Pour la rendre <strong>militante</strong> !</p><p></p><p>Si cela colle entre nous, nous pourrons poursuivre l&#39;événement autour d&#39;un verre :)</p>
    """,
    lyon,
    ~U[2026-11-05 16:00:00Z],
    ~U[2026-11-05 21:00:00Z],
    %{comment_moderation: :closed},
    picture,
    [],
    "fr",
    "MOVEMENTS_POLITICS",
    [collage, streetart]
  )

Demo.create_participant(collage_fr, gardenia_boreal)
Demo.create_participant(collage_fr, bergamote_austral)
Demo.create_participant(collage_fr, valerian_austral)
Demo.create_participant(collage_fr, jasmin_boreal)
Demo.create_participant(collage_fr, narcisse_boreal)
Demo.create_participant(collage_fr, lily_austral)
Demo.create_participant(collage_fr, calendula_austral)

Demo.create_comment(
  lily_austral,
  collage_fr,
  "<p>Je suis carrément partante ! J'apporte la colle et mes dessins !<br/></p>"
)

Demo.create_comment(valerian_austral, collage_fr, "<p>Chouette initiative. J'en suis !<br/></p>")
Demo.create_comment(calendula_austral, collage_fr, "<p>J'aime beaucoup l'idée, oui.</p>")
Demo.create_comment(gardenia_boreal, collage_fr, "<p>J'en suis aussi !<br/></p>")

Demo.create_comment(
  narcisse_boreal,
  collage_fr,
  "<p>Ça me plait bien aussi. J'espère que le temps sera avec nous.<br/></p>"
)

Demo.create_comment(
  narcisse_boreal,
  collage_fr,
  "<p>Les projets de votre groupe me plaisent beaucoup. J'aimerais faire partie des Contributopistes :)</p>"
)

tower_london = Demo.search_address("Tower of London")

picture = Demo.upload_media(rose_utopia, "2020-10-06-mobilizon-illustration-D_realisation.jpg")
media = Demo.upload_media(rose_utopia, "urban_collage-avatar.jpg")
collage = Demo.create_tag("collage")
streetart = Demo.create_tag("streetart")
street = Demo.create_tag("street")
art = Demo.create_tag("art")
progressive = Demo.create_tag("progressive")

collage_en =
  Demo.create_event(
    rose_utopia,
    the_contributopists,
    "Another collage on the wall",
    """
    <h1>Because the street belongs to everyone, it also belongs to all of us!</h1><p>Let&#39;s take the opportunity to <strong>embellish</strong> it! To make it <strong>political</strong>! To make it <strong>militant</strong>!</p><p></p><p>We have to stick together! Whether you simply want to embellish the walls with your drawings or spread a progressive and inclusive message, or even just come and see: <strong>join the happy team</strong>!</p><p></p><p><img src="#{media.file.url}"/></p>
    """,
    tower_london,
    ~U[2026-11-05 17:00:00Z],
    ~U[2026-11-05 22:00:00Z],
    %{comment_moderation: :closed},
    picture,
    [media],
    "en",
    "MOVEMENTS_POLITICS",
    [collage, streetart, street, art, progressive]
  )

Demo.create_participant(collage_en, valerian_austral)
Demo.create_participant(collage_en, lily_austral)
Demo.create_participant(collage_en, petunia_austral)
Demo.create_participant(collage_en, narcisse_boreal)
Demo.create_participant(collage_en, acacia_boreal)
Demo.create_participant(collage_en, gardenia_boreal)

Demo.create_comment(
  narcisse_boreal,
  collage_en,
  "<p>I&#39;d like to join your group! You seem to do nice things :)</p>"
)

Demo.create_comment(narcisse_boreal, collage_en, "<p>I&#39;m looking forward to it!</p>")
Demo.create_comment(gardenia_boreal, collage_en, "<p>I hope the weather will be nice!</p>")
Demo.create_comment(lily_austral, collage_en, "<p>Let me grab my glue and my drawings!</p>")
Demo.create_comment(valerian_austral, collage_en, "<p>Nice idea! I&#39;m in!<br/></p>")

# Plus d'arbres

matabiau = Demo.search_address("Gare Matabiau, Toulouse")
picture = Demo.upload_media(benedicte_verte, "khadeeja-yasser-YV_ZEsnVHvc-unsplash.jpg")
media1 = Demo.upload_media(benedicte_verte, "mob-25-09.webp")
media2 = Demo.upload_media(benedicte_verte, "noah-buscher-x8ZStukS2PM-unsplash.jpg")

anti_pub = Demo.create_tag("anti-pub")
contribution = Demo.create_tag("contribution")
utopie = Demo.create_tag("utopie")
surveillance = Demo.create_tag("surveillance")
toulouse = Demo.create_tag("toulouse")

plus_arbres =
  Demo.create_event(
    benedicte_verte,
    les_contributopistes,
    "Plus d'arbres, moins de Pub !",
    """
    <h1>Cachons les écran de pub-surveillance !</h1><h2>Des écrans qui nous surveillent</h2><p>Ces écrans polluent notre ville, nos espaces publics et nos cerveaux.</p><p>Mais le pire, c&#39;est qu&#39;<strong>ils nous observent, et prennent des notes</strong>.</p><p></p><p>En effet, les capteurs intégrés à ces écrans publicitaires essaient de mesurer nos regards et nos réactions aux publicités émises !</p><p></p><p><img src="#{media1.file.url}"/></p><p></p><h2>Tournez le dos à la pub (avec un parapluie)</h2><p>Nous rappelons qu&#39;il est interdit de vandaliser ces écrans publicitaires.</p><p>Par contre, <strong>rien ne vous empêche de vous poser devant avec le plus grand parapluie de votre collection,</strong> pour mieux gâcher la vue aux capteurs.</p><p>Ces moments de blocage publicitaire humain sont souvent un temps de convivialité où vous pourrez expliquer aux passant·es en quoi vous être en train de protéger leur vie privée !</p><p></p><p><img src="#{media2.file.url}"/></p><p></p><h2>Un blocage de pub = un arbre ! </h2><p>L&#39;idée de cet événement est d&#39;interpeller les personnes passant dans le lieu public. Pour les inciter à contribuer à notre action, vous pouvez leur proposer de <strong>tenir le parapluie quelques minutes en échange d&#39;une bouture d&#39;arbre</strong> !</p><p></p><p>Nous avons prévus de nombreuses pousses à distribuer tout au long de la journée, espérons que les passant·es joueront le jeu !</p><p></p><p></p>
    """,
    matabiau,
    ~U[2026-11-07 07:00:00Z],
    ~U[2026-11-07 11:00:00Z],
    %{comment_moderation: :closed},
    picture,
    [media1, media2],
    "fr",
    "MOVEMENTS_POLITICS",
    [anti_pub, contribution, utopie, surveillance, toulouse]
  )

Demo.create_participant(plus_arbres, david_bouleau)
Demo.create_participant(plus_arbres, geraldine_erable)

{:ok, anonymous_actor} = Mobilizon.Actors.get_or_create_internal_actor("anonymous")

Demo.create_participant(plus_arbres, anonymous_actor, %{
  "email" => "spf+roseanon@framasoft.org",
  "locale" => "fr",
  "message" => "Eh ! J'en suis !\\n(et j&#39;ai un parapluie :) )",
  "cancellation_token" => "3x3Fhb8MOXGF4hk4E7HABwfsoH9-eZqANo1E1nwl",
  "confirmation_token" => "mM_ABQjp8XIso3q8Lwtr3y-_PtPRCByp21kHzYAa"
})

Demo.create_comment(
  rose_utopia,
  plus_arbres,
  "<p>Eh ! C'était super ! Est-ce que je pourrais rejoindre le groupe ?</p>"
)

brighton_station = Demo.search_address("Brighton Station, Hove")
picture = Demo.upload_media(benedict_green, "khadeeja-yasser-YV_ZEsnVHvc-unsplash.jpg")
media1 = Demo.upload_media(benedict_green, "mob-25-09.webp")
media2 = Demo.upload_media(benedict_green, "noah-buscher-x8ZStukS2PM-unsplash.jpg")

contribution = Demo.create_tag("contribution")
surveillance = Demo.create_tag("surveillance")
green = Demo.create_tag("green")
adblock = Demo.create_tag("adblock")
utopia = Demo.create_tag("utopia")

more_trees =
  Demo.create_event(
    benedict_green,
    the_contributopists,
    "More trees, less ads",
    """
    <h1>Let&#39;s hide the advertising-monitoring screens!</h1><h2>Screens that monitor us</h2><p></p><p>These screens pollute our city, our public spaces and our brains.</p><p>But the worst thing is that <strong>they observe us and take notes.</strong></p><p>Indeed, the sensors integrated into these advertising screens try to measure our looks and our reactions to the advertisements that are shown!</p><p><img src="#{media1.file.url}"/></p><h2>Turn your back to the ad (with an umbrella)</h2><p>We remind you that it is forbidden to vandalize these advertising screens.</p><p>On the other hand, nothing prevents you from <strong>standing in front of those screens with the largest umbrella</strong> in your collection, to better spoil the view to the sensors.</p><p>These moments of human advertising blockage are often a time of conviviality where you can explain to passers-by how you are protecting their privacy!</p><p><img src="#{media2.file.url}"/></p><h2>An ad block = a tree!</h2><p>The idea of this event is to challenge people passing in the public place. To incite them to contribute to our action, you can offer them to <strong>hold the umbrella for a few minutes in exchange for a sapling</strong>!</p><p>We have many shoots to distribute throughout the day, hopefully passers-by will play the game!</p><p></p>
    """,
    brighton_station,
    ~U[2026-11-07 06:00:00Z],
    ~U[2026-11-07 10:00:00Z],
    %{comment_moderation: :closed},
    picture,
    [media1, media2],
    "en",
    "MOVEMENTS_POLITICS",
    [contribution, surveillance, green, adblock, utopia]
  )

Demo.create_participant(more_trees, david_birch)
Demo.create_participant(more_trees, geraldine_mapple)

Demo.create_participant(more_trees, anonymous_actor, %{
  "email" => "spf+rose@framasoft.org",
  "locale" => "en",
  "message" => "Hi! I&#39;m in!\\n(And I have an umbrella ;) )",
  "cancellation_token" => "qaeh2Fp1WaBz-g2YjKX25zVM80gbwBIp-LD0nb_a",
  "confirmation_token" => "6EJh9t7sO5HU-ieZ0nKoQCOTjLmhxWd31lpugEwl"
})

Demo.create_comment(
  rose_utopia,
  more_trees,
  "<p>Eh! That was a great event! Can I join your group?</p>"
)

### Posts
picture = Demo.upload_media(les_contributopistes, "code-of-conduct.jpg")

coc =
  Demo.create_post(
    benedicte_verte,
    les_contributopistes,
    "Code de Conduite des Contributopistes",
    """
    <h2>La version courte</h2><p>L'orga des Contributopistes s’engage à bannir toute forme de harcèlement quel qu’il soit et à garantir la même expérience à tous, indépendamment du genre, de l’orientation sexuelle, du handicap, de l’apparence physique, de la taille, de l'ethnie ou de la religion.</p><p></p><p>Nous ne tolérerons aucune forme d’attaque personnelle quelle qu’elle soit. Les participants à l’événement qui violeraient ces principes pourront être sanctionnés et expulsés à la discrétion des organisateurs.</p><p></p><h2>La version moins courte</h2><p>Le harcèlement inclut les injures et remarques offensives relatives au genre, à l’orientation sexuelle, aux handicaps, à l’apparence physique, à la taille, l'ethnie ou la religion, ainsi que la diffusion d’images sexuelles dans les espaces publics, l’intimidation délibérée, le fait de suivre quelqu’un de façon persistante, de prendre des photos ou enregistrements contre la préférence des personnes concernées, l’interruption abusive de conversations, sessions ou autres événements, les contacts physiques inappropriés, ou encore toute attention sexuelle non souhaitée. Cette liste ne doit pas être considérée comme exhaustive.</p><p></p><p>Tout participant auquel on demande de cesser un comportement de harcèlement doit obtempérer immédiatement.</p><p></p><p>Si un participant se rend coupable de harcèlement, les organisateurs sont en droit de prendre toute mesure qu’ils estimeraient appropriée à son encontre, du simple avertissement à l’expulsion pure et simple de l’événement.</p><p></p><p>Si vous êtes harcelé·e (ou remarquez quelqu’un qui semble l’être) ou que vous avez la moindre question ou remarque relative à ce code de conduite, informez-en immédiatement un staffeur qui transmettra le message au responsable de l’équipe organisatrice. Nous indiquerons également le nom des personnes responsables de cet aspect au début de l’événement : n’hésitez pas à les contacter directement.</p><p></p><p>Nous sommes à la disposition des participants pour faire intervenir l’équipe de sécurité du lieu de l’événement ou les forces de l’ordre locales et de façon générale assister ceux qui sont en situation de harcèlement afin de leur permettre de se sentir en sécurité. Il est important pour nous que tous les participants prennent part à l’événement en toute sérénité.</p><p></p><p>Ces règles sont applicables pendant nos événements et tout rassemblement social qui y est associé, tels qu’un drinkup dans un bar, une soirée, etc.</p>
    """,
    picture
  )

picture = Demo.upload_media(les_contributopistes, "3_Educ-Pop_by-David-Revoy.jpg")

Demo.create_post(
  benedicte_verte,
  les_contributopistes,
  "Devenez Contributopistes !",
  """
  <h1>Vous voulez changer le monde, un humain à la fois ?</h1><p></p><p>Ça tombe bien, nous aussi !</p><p>Les ContribUtopistes est un groupe d'actions non-violentes visant à concrétiser une société de contribution.</p><p>Si cela vous intéresse, <strong>la première étape est de lire notre </strong><a href="#{coc.url}" rel="noopener noreferrer nofollow"><strong>code de conduite</strong></a> (et voir si vous y adhérez).</p><p>Si c'est le cas, venez librement parler avec nous lors d'un de nos événements ou demandez à rejoindre notre groupe Mobilizon.</p><p></p><h2>Pour rejoindre notre groupe :</h2><ul><li><p>Créez vous un compte sur une instance Mobilizon</p></li><li><p>Choisissez le profil que vous voulez inscrire</p></li><li><p>Commentez sous un de nos événements (<a href="#{plus_arbres.url}" rel="noopener noreferrer nofollow">par exemple celui-ci</a>) pour demander à être invité·e au groupe</p></li></ul>
  """,
  picture
)

picture = Demo.upload_media(les_contributopistes, "code-of-conduct.jpg")
coc_tag = Demo.create_tag("Code of conduct")
contribution = Demo.create_tag("contribution")
utopia = Demo.create_tag("utopia")

coc_en =
  Demo.create_post(
    benedict_green,
    the_contributopists,
    "The ContribUtopists Code of Conduct",
    """
    <h2> 1. Purpose</h2><p></p><p>A primary goal of The ContribUtopists is to be inclusive to the largest number of contributors, with the most varied and diverse backgrounds possible. As such, we are committed to providing a friendly, safe and welcoming environment for all, regardless of gender, sexual orientation, ability, ethnicity, socioeconomic status, and religion (or lack thereof).</p><p></p><p>This code of conduct outlines our expectations for all those who participate in our community, as well as the consequences for unacceptable behavior.</p><p>We invite all those who participate in The ContribUtopists to help us create safe and positive experiences for everyone.</p><p></p><h2>2. Open [Source/Culture/Tech] Citizenship</h2><p></p><p>A supplemental goal of this Code of Conduct is to increase open [source/culture/tech] citizenship by encouraging participants to recognize and strengthen the relationships between our actions and their effects on our community.</p><p>Communities mirror the societies in which they exist and positive action is essential to counteract the many forms of inequality and abuses of power that exist in society.</p><p></p><p>If you see someone who is making an extra effort to ensure our community is welcoming, friendly, and encourages all participants to contribute to the fullest extent, we want to know.</p><p></p><h2>3. Expected Behavior</h2><p></p><p>The following behaviors are expected and requested of all community members:</p><ul><li><p> Participate in an authentic and active way. In doing so, you contribute to the health and longevity of this community. </p></li><li><p> Exercise consideration and respect in your speech and actions.</p></li><li><p> Attempt collaboration before conflict.</p></li><li><p> Refrain from demeaning, discriminatory, or harassing behavior and speech.</p></li><li><p> Be mindful of your surroundings and of your fellow participants. Alert community leaders if you notice a dangerous situation, someone in distress, or violations of this Code of Conduct, even if they seem inconsequential.</p></li><li><p> Remember that community event venues may be shared with members of the public; please be respectful to all patrons of these locations.</p></li></ul><h2> 4. Unacceptable Behavior</h2><p>The following behaviors are considered harassment and are unacceptable within our community:</p><ul><li><p> Violence, threats of violence or violent language directed against another person.</p></li><li><p> Sexist, racist, homophobic, transphobic, ableist or otherwise discriminatory jokes and language.</p></li><li><p> Posting or displaying sexually explicit or violent material.</p></li><li><p> Posting or threatening to post other people's personally identifying information ("doxing").</p></li><li><p> Personal insults, particularly those related to gender, sexual orientation, race, religion, or disability.</p></li><li><p> Inappropriate photography or recording.</p></li><li><p> Inappropriate physical contact. You should have someone's consent before touching them.</p></li><li><p> Unwelcome sexual attention. This includes, sexualized comments or jokes; inappropriate touching, groping, and unwelcomed sexual advances.</p></li><li><p> Deliberate intimidation, stalking or following (online or in person).</p></li><li><p> Advocating for, or encouraging, any of the above behavior.</p></li><li><p> Sustained disruption of community events, including talks and presentations.</p></li></ul><p></p><h2> 5. Weapons Policy</h2><p></p><p>No weapons will be allowed at The ContribUtopists events, community spaces, or in other spaces covered by the scope of this Code of Conduct. Weapons include but are not limited to guns, explosives (including fireworks), and large knives such as those used for hunting or display, as well as any other item used for the purpose of causing injury or harm to others. Anyone seen in possession of one of these items will be asked to leave immediately, and will only be allowed to return without the weapon. Community members are further expected to comply with all state and local laws on this matter.</p><p></p><h2> 6. Consequences of Unacceptable Behavior</h2><p></p><p>Unacceptable behavior from any community member, including sponsors and those with decision-making authority, will not be tolerated.</p><p>Anyone asked to stop unacceptable behavior is expected to comply immediately.</p><p>If a community member engages in unacceptable behavior, the community organizers may take any action they deem appropriate, up to and including a temporary ban or permanent expulsion from the community without warning (and without refund in the case of a paid event).</p><p></p><h2> 7. Reporting Guidelines</h2><p></p><p>If you are subject to or witness unacceptable behavior, or have any other concerns, please notify a community organizer as soon as possible.</p><p>Additionally, community organizers are available to help community members engage with local law enforcement or to otherwise help those experiencing unacceptable behavior feel safe. In the context of in-person events, organizers will also provide escorts as desired by the person experiencing distress.</p><p></p><h2> 8. Addressing Grievances</h2><p></p><p>If you feel you have been falsely or unfairly accused of violating this Code of Conduct, you should notify admins with a concise description of your grievance. Your grievance will be handled in accordance with our existing governing policies.</p><p></p><p></p><h2> 9. Scope</h2><p></p><p>We expect all community participants (contributors, paid or otherwise; sponsors; and other guests) to abide by this Code of Conduct in all community venues--online and in-person--as well as in all one-on-one communications pertaining to community business.</p><p>This code of conduct and its related procedures also applies to unacceptable behavior occurring outside the scope of community activities when such behavior has the potential to adversely affect the safety and well-being of community members.</p>
    """,
    picture,
    [],
    [coc_tag, contribution, utopia]
  )

contribution = Demo.create_tag("contribution")
utopia = Demo.create_tag("utopia")
join = Demo.create_tag("join")
english = Demo.create_tag("english")

Demo.create_post(
  benedict_green,
  the_contributopists,
  "Join the ContribUtopists!",
  """
  <h1>You want to change the world, one human at a time?</h1><p>Well, so do we!</p><p>The ContribUtopists is a group of non-violent actions aimed at creating a society of contribution.</p><p>If you are interested, <strong>the first step is to read </strong><a href="#{coc_en.url}" rel="noopener noreferrer nofollow"><strong>our code of conduct</strong> </a>(and see if you adhere to it).</p><p>If you do, come and speak freely with us at one of our events or ask to join our Mobilizon group.</p><h2>To join our group :</h2><ul><li><p>Create an account on a Mobilizon instance</p></li><li><p>Choose the profile you want to register</p></li><li><p>Comment under one of our events (<a href="#{more_trees.url}" rel="noopener noreferrer nofollow">for example this one</a>) to ask to be invited to the group</p></li></ul><p></p>
  """,
  nil,
  [],
  [contribution, utopia, join, english]
)
